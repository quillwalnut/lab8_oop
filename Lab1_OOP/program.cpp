#include "stdafx.h"
#include "program.h"
#include <string>

using namespace std;

namespace simple_shapes
{
	shape* shape::In(ifstream &in)
	{
		shape *sp;
		int k;
		in >> k;
		switch (k)
		{
		case 1:
			sp = new aphorism;
			break;
		case 2:
			sp = new proverb;
			break;
		case 3:
			sp = new riddle;
			break;
		default:
			return 0;
		}
		string temp;
		getline(in, temp); // ������� ������ �������� �� ����� ������
		sp->InData(in);
		getline(in, sp->text);
		in >> sp->assessment;
		return sp;
	}

	int shape::Count()
	{
		int result = 0;
		for (int i = 0; i < text.length(); i++)
		if (text[i] == ',' || text[i] == '.' || text[i] == '?' ||
			text[i] == '!' || text[i] == '(' || text[i] == ')' ||
			text[i] == '"' || text[i] == '-' || text[i] == ':')
			result++;
		return result;
	}

	void aphorism::InData(ifstream &in)
	{		
		getline(in, author);
	}

	void proverb::InData(ifstream &in)
	{
		getline(in, country);
	}

	void riddle::InData(ifstream &in)
	{
		getline(in, answer);
	}

	void aphorism::Out(ofstream &out)
	{
		out << "It is aphorism: author = " << author << ", text = "
			<<  text << ", Subjective assessment - " << assessment
			<< ", count of punctuation marks = " << Count() << endl;
	}

	void proverb::Out(ofstream &out)
	{
		out << "It is proverb: country = " << country << ", text = "
			<< text << ", Subjective assessment - " << assessment
			<< ", count of punctuation marks = " << Count() << endl;
	}

	void riddle::Out(ofstream &out)
	{
		out << "It is riddle: answer = " << answer << ", text = "
			<< text << ", Subjective assessment - " << assessment
			<< ", count of punctuation marks = " << Count() << endl;
	}

	void List::Clear()
	{
		while (size != 0) //���� ����������� ������ �� ������ ������� 
		{
			Node *temp = Head->Next;
			delete Head; //����������� ������ �� ��������� ��������
			Head = temp; //����� ������ ������ �� ����� ���������� ��������
			size--; //���� ������� ����������. ������������ ����� ���������
		}
	}

	void List::In(ifstream &in)
	{
		while (!in.eof())
		{
			size++; //��� ������ ���������� �������� ����������� ����� ��������� � ������
			Node  *temp = new Node; //��������� ������ ��� ������ �������� ������
			temp->Next = Head; //��������� �������. ��������� ������� - ��� ������ ������ 
			temp->x = shape::In(in); //���������� � ���������� ������ ������ �������� x 

			if (Head != NULL) //� ��� ������ ���� ������ �� ������
			{
				Tail->Next = temp; //������ ������ � ��������� �� ��������� ��������� ����
				Tail = temp; //��������� �������� �������=������ ��� ���������.
			}
			else Head = Tail = temp;//���� ������ ���� �� ��������� ������ �������.
		}
	}

	void List::Out(ofstream &out)
	{
		out << "Container contains " << size
			<< " elements." << endl;
		Node *tempHead = Head; //��������� �� ������

		int temp = size; //��������� ���������� ������ ����� ��������� � ������
		int i = 0;
		while (temp != 0) //���� �� �������� ������� ������� �� ����� ������
		{
			out << i << ": ";
			i++;
			tempHead->x->Out(out); //��������� ������� ������ �� ����� 
			tempHead = tempHead->Next; //���������, ��� ����� ��������� �������
			temp--; //���� ������� ������, ������ �������� �� ���� ������ 
		}
	}

	bool shape::Compare(shape &other)
	{
		return Count() < other.Count();
	}

	void List::Sort()
	{
		Node *p = Head;
		for (int i = 0; i < size - 1; i++)
		{
			Node *temp = p->Next;
			for (int j = i + 1; j < size; j++)
			{
				if (p->x->Compare(*temp->x))
				{
					shape *tmp = p->x;
					p->x = temp->x;
					temp->x = tmp;
				}
				temp = temp->Next;
			}
			p = p->Next;

		}
	}

	void shape::OutAphorisms(ofstream &out)
	{
		out << endl;
	}

	void aphorism::OutAphorisms(ofstream &out)
	{
		Out(out);
	}

	void List::OutAphorisms(ofstream &out)
	{
		Node *tempHead = Head; //��������� �� ������
		out << "Only aphorisms." << endl;
		int temp = size; //��������� ���������� ������ ����� ��������� � ������
		int i = 0;
		while (temp != 0) //���� �� �������� ������� ������� �� ����� ������
		{
			out << i << ": ";
			i++;
			tempHead->x->OutAphorisms(out); //��������� ������� ������ �� ����� 
			tempHead = tempHead->Next; //���������, ��� ����� ��������� �������
			temp--; //���� ������� ������, ������ �������� �� ���� ������ 
		}
	}

	List::List() :Head(NULL), Tail(NULL), size(0) {};
}